export default interface Settings{
    x: number,
    y: number,
    width: number,
    height: number,
    orientation:string,
    commands: string
}
